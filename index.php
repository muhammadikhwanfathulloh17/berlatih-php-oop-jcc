<?php

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun", "4", "no");

echo $sheep->getName(); 
echo $sheep->getLegs(); 
echo $sheep->getCold_blooded(); 
echo "<br>";

$sungokong = new Ape("kera sakti", "2", "no");

echo $sungokong->getName(); 
echo $sungokong->getLegs(); 
echo $sungokong->getCold_blooded(); 
echo $sungokong->yell(); 
echo "<br>";

$kodok = new Frog("buduk", "4", "no");

echo $kodok->getName(); 
echo $kodok->getLegs(); 
echo $kodok->getCold_blooded(); 
echo $kodok->jump(); 
echo "<br>";

?>