<?php

class Animal{
  private $name;
  private $legs;
  private $cold_blooded;
  
  public function __construct($name, $legs, $cold_blooded)
  {
    $this->name = $name;
    $this->legs = $legs;
    $this->cold_blooded = $cold_blooded;
  }
 
  public function getName()
  {
    return "Name : ".$this->name."<br>";
  }
 
  public function getLegs()
  {
    return "Legs : ".$this->legs."<br>";
  }
 
  public function getCold_blooded()
  {
    return "Cold blooded : ".$this->cold_blooded."<br>";
  }
}

?>
